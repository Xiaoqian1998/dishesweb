let Dishes = require('../models/dishes');
let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');

const mongodbUri = "mongodb://lxq:qmlpTrVYVmTqZ58s@cluster0-shard-00-00-szspm.mongodb.net:27017,cluster0-shard-00-01-szspm.mongodb.net:27017,cluster0-shard-00-02-szspm.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
mongoose.connect(mongodbUri);
let db = mongoose.connection;

db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});

db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ] on mlab.com');
});

router.findAll = (req, res) => {
    // Return a JSON representation of our list
    res.setHeader('Content-Type', 'application/json');

    Dishes.find(function(err, dishes) {
        if (err)
            res.send(err);

        res.send(JSON.stringify(dishes,null,5));
    });
}

router.findOne = (req, res) => {
    res.setHeader('Content-Type', 'application/json');

    Dishes.find({ "_id" : req.params.id },function(err, dishes) {
        if (err) {
            res.json({message: 'Dishes NOT Found!'});
        }
        else
            res.send(JSON.stringify(dishes,null,5));
    });
}

router.addDishes = (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    var dishes = new Dishes();
    dishes.name = req.body.name;
    dishes.code = req.body.code;
    dishes.price = req.body.price;
    dishes.save(function(err) {
        if (err)
            res.json({ message: 'Dishes NOT Added!', errmsg : err } );
        else
            res.json({ message: 'Dishes Successfully Added!'});
    });
}

router.incrementUpvotes = (req, res) => {
    Dishes.findById(req.params.id, function(err,dishes) {
        if (err) {
            res.status(404);
            res.json({message: 'Invalid Dishes Id!'});
        }
        else {
            dishes.upvotes += 1;
            dishes.save(function (err) {
                if (err)
                    res.json({ message: 'Dishes NOT UpVoted!', errmsg : err } );
                else
                    res.json({ message: 'Dishes Successfully Upvoted!', data: dishes });
            });
        }
    });
}

router.reductionUpvotes = (req, res) => {
    Dishes.findById(req.params.id, function(err,dishes) {
        if (err)
            res.json({ message: 'Dishes NOT Found!', errmsg : err } );
        else {
            if(dishes.upvotes < 1) return;
            dishes.upvotes -= 1;
            dishes.save(function (err) {
                if (err)
                    res.json({ message: 'Dishes NOT Downvoted!', errmsg : err } );
                else
                    res.json({ message: 'Dishes Successfully Downvoted!', data: dishes });
            });
        }
    });
}

router.deleteDishes = (req, res) => {
    Dishes.findByIdAndRemove(req.params.id, function(err) {
        if (err)
            res.json({ message: 'Dishes NOT DELETED!', errmsg : err } );
        else
            res.json({ message: 'Dishes Successfully Deleted!'});
    });
}

module.exports = router;