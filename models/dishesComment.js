let mongoose = require('mongoose');

let DishesCommentSchema = new mongoose.Schema({
        code: String,
        title: String,
        message: String,
        upvotes: {type: Number, default: 0}
    },
    { collection: 'dishedComment' });

module.exports = mongoose.model('DishesComment', DishesCommentSchema);